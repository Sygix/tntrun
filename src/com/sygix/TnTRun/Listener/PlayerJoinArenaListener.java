package com.sygix.TnTRun.Listener;

import java.sql.SQLException;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;

import com.sygix.TnTRun.Arena;
import com.sygix.TnTRun.QuicksandPlugin;
import com.sygix.TnTRun.Events.PlayerJoinArenaEvent;
import com.sygix.TnTRun.Helper.Metadata;
import com.sygix.TnTRun.SQL.SQLGestion;

public class PlayerJoinArenaListener implements Listener
{
	private final QuicksandPlugin plugin;

	public PlayerJoinArenaListener(QuicksandPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerJoinArena(PlayerJoinArenaEvent event)
	{
		Player player = event.getPlayer();
		Arena arena = event.getArena();

		if(arena != null){
			if (!arena.getSettingsApi().isArenaReady()) {
				arena.announce(ChatColor.RED + "Arena " + arena.getTitle() + "hasn't been set up yet! You need a lobby, spawn, and end!", player);
				return;
			}
		}

		if (player.getGameMode() == GameMode.CREATIVE) {
			arena.announce(ChatColor.RED + "Vous ne pouvez pas rejoindre le jeu en cr�atif !", player);
			return;
		}

		if (arena.getMatchApi().isRunning()) {
			arena.announce(ChatColor.RED + "Une partie est d�j� en cours !", player);
			return;
		}

		if (arena.getMatchApi().isContestant(player)) {
			arena.announce(ChatColor.RED + "Vous �tes d�j� dans la queue de ce jeu !", player);
			return;
		}

		if (Metadata.isset(player, "arena") && this.plugin.getArena(Metadata.asString(player, "arena")).getMatchApi().isContestant(player)) {
			arena.announce(ChatColor.RED + "Vous �tes d�j� dans une autre ar�ne !", player);
			return;
		}

		arena.getMatchApi().addContestant(player);
		arena.announce(player.getName() + " a rejoint la partie !");
		try {
			double phalo = SQLGestion.getHalo(player);
			if(phalo > QuicksandPlugin.maxhalo){
				QuicksandPlugin.maxhalo = phalo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		int numPlayers = arena.getMatchApi().countContestants();
		int minPlayers = arena.getSettingsApi().getMinPlayers();

		if (numPlayers < minPlayers) {
			arena.announce(ChatColor.YELLOW +"Il manque encore "+ String.valueOf(minPlayers - numPlayers) + " joueur(s) avant que la partie ne commence !");
			for (PotionEffect effect : player.getActivePotionEffects()){
		        player.removePotionEffect(effect.getType());
			}
			player.setAllowFlight(false);
			player.setFlying(false);
			return;
		}

		if (numPlayers == minPlayers) {
			arena.startMatch();
		}
		for (PotionEffect effect : player.getActivePotionEffects()){
	        player.removePotionEffect(effect.getType());
		}
		player.setAllowFlight(false);
		player.setFlying(false);
		arena.getPlayerApi().teleport(player, "spawn");
	}
}
