package com.sygix.TnTRun.Listener;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sygix.TnTRun.Arena;
import com.sygix.TnTRun.QuicksandPlugin;
import com.sygix.TnTRun.Helper.Metadata;
import com.sygix.TnTRun.Helper.giveCoins;

public class BlockListener implements Listener
{
	private final QuicksandPlugin plugin;

	public BlockListener(QuicksandPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerRun(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();

		if (!Metadata.isset(player, "arena"))
			return;

		Arena arena = this.plugin.getArena(Metadata.asString(player, "arena"));

		if (!arena.getMatchApi().isRunning() || !arena.getMatchApi().isActiveContestant(player))
			return;

		Block playerBlock = player.getLocation().getBlock();

		if ((this.hasDelta(event.getFrom().getBlockX(), event.getTo().getBlockX()) || this.hasDelta(event.getFrom().getBlockZ(), event.getTo().getBlockZ())) && (event.getFrom().getBlockY() == event.getTo().getBlockY())) {
			player.setFoodLevel(20);
			arena.addMovingPlayer(player);
			arena.breakBlock(event.getFrom().getBlock().getRelative(BlockFace.DOWN));
		}

		if (playerBlock.isLiquid()) {
			arena.announce(ChatColor.GREEN + player.getName() + " a perdu !");
			arena.getMatchApi().changeSpectatorMode(player, true);
			giveCoins.coins(player, null);
			if (arena.getMatchApi().countActiveContestants() <= 1) {
				arena.announceTheWinner();
				arena.reset();
			} else {
				arena.announce(ChatColor.RED + "Vous avez perdu, vous �tes maintenant un spectateur !", player);
				arena.getPlayerApi().teleport(player, "spawn");
			}
		}
	}

	private boolean hasDelta(int pos1, int pos2)
	{
		return Math.abs(pos1 - pos2) == 1;
	}
}
