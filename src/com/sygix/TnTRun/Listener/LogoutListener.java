package com.sygix.TnTRun.Listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sygix.TnTRun.QuicksandPlugin;
import com.sygix.TnTRun.Events.PlayerLeaveEvent;

public class LogoutListener implements Listener
{
	private final QuicksandPlugin plugin;

	public LogoutListener(QuicksandPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e)
	{
		this.plugin.getServer().getPluginManager().callEvent(new PlayerLeaveEvent(e.getPlayer()));
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		this.plugin.getServer().getPluginManager().callEvent(new PlayerLeaveEvent(e.getPlayer()));
	}
}
