package com.sygix.TnTRun.Listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.sygix.TnTRun.Arena;
import com.sygix.TnTRun.QuicksandPlugin;
import com.sygix.TnTRun.Events.PlayerLeaveEvent;
import com.sygix.TnTRun.Helper.Metadata;

public class PlayerLeaveListener implements Listener
{
	private final QuicksandPlugin plugin;

	public PlayerLeaveListener(QuicksandPlugin plugin)
	{
		this.plugin = plugin;
	}

	@EventHandler
	public void onPlayerLeave(PlayerLeaveEvent event)
	{
		Player player = event.getPlayer();

		if (!Metadata.isset(player, "arena")) {
			this.plugin.getChatApi().announce(ChatColor.RED + "Vous ne jouez pas !", player);
			return;
		}

		Arena arena = this.plugin.getArena(Metadata.asString(player, "arena"));

		if (!arena.getMatchApi().isContestant(player)) {
			Metadata.remove(player, "arena");
			return;
		}

		arena.getMatchApi().removeContestant(player);
		if (arena.getMatchApi().countContestants() == 0) {
			arena.reset();
		}

		if (arena.getMatchApi().isRunning()) {
			arena.announce(ChatColor.GREEN + "Merci d'avoir jou� !", player);
		} else {
			arena.announce(ChatColor.GREEN + "Merci de votre visite !", player);
		}
	}
}
