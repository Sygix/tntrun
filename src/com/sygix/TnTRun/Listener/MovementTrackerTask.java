package com.sygix.TnTRun.Listener;

import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.TnTRun.Arena;

public class MovementTrackerTask extends BukkitRunnable
{
	private final Arena arena;

	public MovementTrackerTask(Arena arena)
	{
		this.arena = arena;
	}

	@Override
	public void run()
	{
		this.arena.shoveCampers();
	}
}
