package com.sygix.TnTRun;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sygix.TnTRun.Helper.Metadata;
import com.sygix.TnTRun.Listener.BlockListener;
import com.sygix.TnTRun.Listener.CheatPrevention;
import com.sygix.TnTRun.Listener.LogoutListener;
import com.sygix.TnTRun.Listener.PlayerJoinArenaListener;
import com.sygix.TnTRun.Listener.PlayerLeaveListener;
import com.sygix.TnTRun.Listener.SignListener;
import com.sygix.TnTRun.SQL.SQLGestion;
import com.sygix.TnTRun.api.ChatApi;


public class QuicksandPlugin extends JavaPlugin
{
	private final Map<String, Arena> arenas = new HashMap<String, Arena>();
	private ChatApi                  chatApi;
	public static double maxhalo = 1.0;

	public Arena getArena(String name)
	{
		if (!this.arenas.containsKey(name.toLowerCase())) {
			Arena arena = new Arena(this, name);
			this.arenas.put(name.toLowerCase(), arena);
		}

		return this.arenas.get(name.toLowerCase());
	}

	public ChatApi getChatApi()
	{
		return this.chatApi;
	}

	@Override
	public void onDisable()
	{
		for (Arena arena : this.arenas.values()) {
			arena.reset();
		}
		try {
			SQLGestion.disconnect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onEnable()
	{
		long start = System.currentTimeMillis();
		this.saveDefaultConfig();

		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(new SignListener(this), this);
		pm.registerEvents(new PlayerJoinArenaListener(this), this);
		pm.registerEvents(new PlayerLeaveListener(this), this);
		pm.registerEvents(new BlockListener(this), this);
		pm.registerEvents(new CheatPrevention(), this);
		pm.registerEvents(new LogoutListener(this), this);

		this.getCommand("tntrun").setExecutor(new QuicksandCommandExecutor(this));
		this.getCommand("tr").setExecutor(new QuicksandCommandExecutor(this));
		this.chatApi = new ChatApi(this, ChatColor.GRAY+""+ChatColor.BOLD+"["+ChatColor.RED+""+ChatColor.BOLD+"TnT"+ChatColor.GRAY+""+ChatColor.BOLD+"Run] " + ChatColor.RESET);

		Metadata.setPlugin(this);
		
		try {
			SQLGestion.connect();
			SQLGestion.registerPlugin();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		this.getLogger().info("charge en " + (System.currentTimeMillis() - start) / 1000 + " seconds.");
	}
}
