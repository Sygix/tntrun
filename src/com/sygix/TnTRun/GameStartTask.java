package com.sygix.TnTRun;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitRunnable;

public class GameStartTask extends BukkitRunnable
{
	private final Arena arena;
	public static int         countdown;

	public GameStartTask(Arena arena, int countdown)
	{
		this.arena = arena;
		GameStartTask.countdown = countdown;
	}

	@Override
	public void run()
	{
		if (GameStartTask.countdown > 0) {
			if (GameStartTask.countdown % 10 == 0 || GameStartTask.countdown < 10) {
				this.arena.getSettingsApi().getLocation("spawn").getWorld().playSound(this.arena.getSettingsApi().getLocation("spawn"), Sound.BLOCK_NOTE_PLING, 2, 2);
				this.arena.announce(ChatColor.GRAY+"D�but dans "+String.valueOf(GameStartTask.countdown));
			}
			GameStartTask.countdown--;
		} else {
			this.arena.getMatchApi().start();
			this.arena.broadcast("La partie a commenc�. " + ChatColor.GREEN + "Bonne chance !");
			this.cancel();
		}
	}
}
