/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.TnTRun.Helper;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.sygix.TnTRun.QuicksandPlugin;
import com.sygix.TnTRun.SQL.SQLGestion;

public class giveCoins {
	
	public static void coins(Player p, String winner){
		p.sendMessage(ChatColor.YELLOW+"------------------------------");
		try{
			if(!p.getName().equals(winner)){ //Perdants
				double coins = SQLGestion.getCoins(p);
				p.sendMessage(ChatColor.DARK_AQUA+"Participation : +1 DirektCoins (x"+QuicksandPlugin.maxhalo+" halo partie)");
				SQLGestion.setCoins(p, coins+(1*QuicksandPlugin.maxhalo));
			}else{ //Winner
				double coins = SQLGestion.getCoins(p);
				p.sendMessage(ChatColor.DARK_AQUA+"Gagnant : +3 DirektCoins (x"+QuicksandPlugin.maxhalo+" halo partie)");
				SQLGestion.setCoins(p, coins+(3*QuicksandPlugin.maxhalo));
			}
		}catch(Exception e){
			p.sendMessage(ChatColor.RED+"Une Erreur est survenue, contactez Sygix.");
			e.printStackTrace();
		}
		p.sendMessage(ChatColor.YELLOW+"------------------------------");
	}

}
