/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.TnTRun.SQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.entity.Player;

public class SQLGestion {
	
	static Connection con = null;

    public static void connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        String url_ = "jdbc:mysql://mc.direktserv.fr/Bungee";
        String pass = "****";
        String user = "root";

        con = DriverManager.getConnection(url_, user, pass);
    }
    
    public static void disconnect() throws SQLException {
    	if(con != null){
    		con.close();
    	}
    }
    
    @SuppressWarnings("unused")
    public static void registerPlugin() throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        try {
            ResultSet res = state.executeQuery("SELECT * FROM players");
        } catch(SQLException e) {
            state.execute("CREATE TABLE players ( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, uid VARCHAR(100), pseudo VARCHAR(100), time LONG, lastconnect LONG, coins DOUBLE, halo DOUBLE, date_buy_halo VARCHAR(100) )");
        }
        state.close();
    }
    
    public static boolean registerOnBDD(Player p) throws SQLException, IOException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='" + p.getUniqueId()+ "'");
        while(!res.next()){
        	Statement s = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            s.executeUpdate("INSERT INTO players (id, uid, pseudo, time, lastconnect, coins, halo, date_buy_halo) VALUES (null, '"+p.getUniqueId()+"', '"+p.getName()+"', 0, 0, 0.0, 1.0, null)");
            state.close();
            return false;
        }
        state.close();
        return true;
    }
    
    public static double getHalo(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static double getHalo(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
        	double halo = res.getDouble("halo");
            state.close();
            return halo;
        }
        state.close();
        return 0.0;
    }
    
    public static void setCoins(Player p, double coins) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
        Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        state.executeUpdate("UPDATE players SET coins="+coins+" WHERE uid='"+p.getUniqueId()+"'");
        state.close();
    }
    
    public static double getCoins(Player p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE uid='"+p.getUniqueId()+"'");
        while(res.next()) {
        	double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }
    
    public static double getCoins(String p) throws SQLException {
    	if(!con.isValid(2)){
    		try {
				connect();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    	}
    	Statement state = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        ResultSet res = state.executeQuery("SELECT * FROM players WHERE pseudo='"+p+"'");
        while(res.next()) {
        	double coins = res.getDouble("coins");
            state.close();
            return coins;
        }
        state.close();
        return 0.0;
    }

}
