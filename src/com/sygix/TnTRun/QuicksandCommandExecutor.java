package com.sygix.TnTRun;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sygix.TnTRun.Events.PlayerJoinArenaEvent;
import com.sygix.TnTRun.Events.PlayerLeaveEvent;

public class QuicksandCommandExecutor implements CommandExecutor
{
	private final QuicksandPlugin plugin;

	public QuicksandCommandExecutor(QuicksandPlugin plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (!cmd.getName().equalsIgnoreCase("tntrun") && !cmd.getName().equalsIgnoreCase("tr"))
			return false;

		if (!(sender instanceof Player))
			return false;

		Player player = (Player) sender;
		Arena arena = args.length > 1 ? this.plugin.getArena(args[1]) : null;

		switch (args.length > 0 ? args[0].toLowerCase() : "help") {
			case "setlobby":
				if (arena == null) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous devez entrer le nom de l'ar�ne !", player);
				} else {
					arena.setup("lobby", player, "quicksand.setlobby", "Vous avez d�finie le lobby.");
				}
				break;
			case "setspawn":
				if (arena == null) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous devez entrer le nom de l'ar�ne !", player);
				} else {
					arena.setup("spawn", player, "quicksand.setspawn", "Vous avez d�finie le spawn en jeu.");
				}
				break;
			case "setend":
				if (arena == null) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous devez entrer le nom de l'ar�ne !", player);
				} else {
					arena.setup("end", player, "quicksand.setend", "Vous avez d�finie le hub/lobby.");
				}
				break;
			case "configure":
				if (!player.hasPermission("quicksand.config")) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous n'avez pas la permission.", player);
					return true;
				}

				if (arena == null) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous devez entrer le nom de l'ar�ne !", player);
					return true;
				}

				if (args.length < 4) {
					this.plugin.getChatApi().announce(ChatColor.RED + "You must enter a settings and its new value!", player);
					return true;
				}

				switch (args[2].toLowerCase()) {
					case "countdown":
						arena.getSettingsApi().setCountdown(Integer.parseInt(args[3]));
						this.plugin.getChatApi().announce("Countdown is set to " + arena.getSettingsApi().getCountdown(), player);
						break;
					case "players":
						arena.getSettingsApi().setMinPlayers(Integer.parseInt(args[3]));
						this.plugin.getChatApi().announce("minPlayers is set to " + arena.getSettingsApi().getMinPlayers(), player);
						break;
					default:
						this.plugin.getChatApi().announce(ChatColor.RED + "Il n'y a pas de r�glage appel� \"" + args[3] + "\"!", player);
						return true;
				}

				break;
			case "join":
				if (player.hasPermission("quicksand.join")) {
					this.plugin.getServer().getPluginManager().callEvent(new PlayerJoinArenaEvent(player, arena));
				} else if (arena == null) {
					this.plugin.getChatApi().announce(ChatColor.RED + "Vous devez entrer le nom de l'ar�ne !", player);
				} else {
					arena.announce(ChatColor.RED + "Vous n'avez pas la permission.", player);
				}
				break;
			case "leave":
				this.plugin.getServer().getPluginManager().callEvent(new PlayerLeaveEvent(player));
				break;
			default:
				player.sendMessage(ChatColor.GOLD + "==========[ " + ChatColor.YELLOW + "TnTRun " + ChatColor.GOLD + "]==========");
				player.sendMessage(ChatColor.RED + "/tntrun setSpawn <arena> " + ChatColor.RESET + "- sets the spawnpoint for the game.");
				player.sendMessage(ChatColor.RED + "/tntrun setLobby <arena> " + ChatColor.RESET + "- sets the waiting room for the game.");
				player.sendMessage(ChatColor.RED + "/tntrun setEnd <arena> " + ChatColor.RESET + "- sets the place where players go at the end of the game.");
				player.sendMessage(ChatColor.RED + "/tntrun configure <arena> countdown <seconds>" + ChatColor.RESET + "- sets the duration of the countdown.");
				player.sendMessage(ChatColor.RED + "/tntrun configure <arena> players <number>" + ChatColor.RESET + "- sets the minimum amount of players required to start the game.");
				player.sendMessage(ChatColor.RED + "/tntrun join <arena> " + ChatColor.RESET + "- join !");
				player.sendMessage(ChatColor.RED + "/tntrun leave " + ChatColor.RESET + "- leave !");
				player.sendMessage(ChatColor.GOLD + "==========[ " + ChatColor.YELLOW + "TnTRun " + ChatColor.GOLD + "]==========");
				break;
		}

		return true;
	}
}
