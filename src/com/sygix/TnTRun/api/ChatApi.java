package com.sygix.TnTRun.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.sygix.TnTRun.QuicksandPlugin;

public class ChatApi
{
	@SuppressWarnings("unused")
	private final QuicksandPlugin plugin;
	private final String          prefix;

	public ChatApi(QuicksandPlugin plugin, String prefix)
	{
		this.plugin = plugin;
		this.prefix = prefix;
	}

	public void announce(String msg, Player... players)
	{
		for (Player player : players) {
			this.tell(player, msg);
		}
	}

	public void broadcast(String msg)
	{
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.sendMessage(this.prefix + msg);
		}
	}

	public void tell(Player player, String msg)
	{
		player.sendMessage(this.prefix + msg);
	}
}
